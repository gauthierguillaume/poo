<?php
abstract class Vehicule
{
    const G= 9.8065;

    protected $masse;
    protected $vitesse;
    protected $dimension; //array()
    protected $rayonCourbure;

    public function calculerEnergieCinetique()
    {
        if ($this->masse > 0)
            return 0.5 * $this->masse * ($this->vitesse ** 2);

        else
            return false;
    }

    public function calculerForceCentrifuge()
    {
        if ($this->rayonCourbure !== 0)
            return self::calculerEnergieCinetique() / $this->rayonCourbure;
        else
            return false;
    }

    public function getMasse()
    {
    return $this->masse;
    }

    public function setMasse(float $m)
    {
        $this->masse = $m;
    }

    public function getVitesse()
    {
        return $this->vitesse;
    }

    public function setVitesse(float $v)
    {
        $this->vitesse = $v;
    }

    public function setRayonCourbure(float $v)
    {
        $this->vitesse = $v;
    }


}
