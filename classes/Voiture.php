<?php
class Voiture extends Vehicule
{
    use Carburant;
    public $couleur = "blanche";
    public $nbreRapport;
    public $nbrePlace;

    public function __construct($place)
    {
        $this->nbrePlace = $place;
    }

    public function renvoyerCouleur()
    {
        return $this->couleur;
    }

    public function __destruct()
    {
        //echo "BOOM !";
    }
}
