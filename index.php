<?php
function debug($array)
{
    echo '<pre>';
    var_dump($array);
    echo '</pre>';
}

include 'functions/autoload.php';
spl_autoload_register('autoload');

//spl_autoload_register(function($classe)) {
//  include 'classes/' . $classe . '.php';
//}

$message = "J'ai mangé gras";
Log::logwrite($message);

$voiture1 = new Voiture(4);

$voiture1->setMasse(1000);
$voiture1->setVitesse(22);

$ec = $voiture1->calculerEnergieCinetique();
echo $ec;

//$voiture1->couleur = "jaune";
//$voiture1->couleur = "rouge";
//
//$voiture2 = new Voiture(4);
//$voiture2->couleur = "verte";
//
//$voiture3 = new Voiture(4);
//
//echo $voiture2->renvoyerCouleur();
//$voiture2->couleur = "Pourpre";
//echo $voiture2->renvoyerCouleur();
//
//debug($voiture1);
//debug($voiture2);
//debug($voiture3);